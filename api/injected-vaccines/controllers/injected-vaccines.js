"use strict";

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */
const { parseMultipartData, sanitizeEntity } = require("strapi-utils");

module.exports = {
  async deleteMultipleInjectedVaccines(ctx) {
    const { IDs } = ctx.params;
    const entity = await strapi.services[
      "injected-vaccines"
    ].deleteMultipleInjectedVaccines(JSON.parse(IDs));
    return sanitizeEntity(entity, {
      model: strapi.models["injected-vaccines"],
    });
  },

  async createMultipleInjectedVaccines(ctx) {
    let entity;
    if (ctx.is("multipart")) {
      const { data, files } = parseMultipartData(ctx);
      entity = await strapi.services[
        "injected-vaccines"
      ].createMultipleInjectedVaccines(data, {
        files,
      });
    } else {
      entity = await strapi.services[
        "injected-vaccines"
      ].createMultipleInjectedVaccines(ctx.request.body);
    }
    return sanitizeEntity(entity, {
      model: strapi.models["injected-vaccines"],
    });
  },
};
