"use strict";

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-services)
 * to customize this service
 */

const { isDraft } = require("strapi-utils").contentTypes;

module.exports = {
  async deleteMultipleInjectedVaccines(IDs) {
    var entriesDeleted = [];

    for (let index = 0; index < IDs.length; index++) {
      // entriesDeleted.push(
      //   strapi.query("injected-vaccines").delete({ id: IDs[index] })
      // );

      entriesDeleted.push(
        await strapi.query("injected-vaccines").delete({ id: IDs[index] })
      );
    }

    return entriesDeleted;
  },

  async createMultipleInjectedVaccines(data, { files } = {}) {
    var entries = [];

    for (let index = 0; index < data.length; index++) {
      const validData = await strapi.entityValidator.validateEntityCreation(
        strapi.models["injected-vaccines"],
        data[index],
        { isDraft: isDraft(data[index], strapi.models["injected-vaccines"]) }
      );

      const entry = await strapi.query("injected-vaccines").create(validData);

      if (files) {
        // automatically uploads the files based on the entry and the model
        await strapi.entityService.uploadFiles(entry, files, {
          model: "injected-vaccines",
          // if you are using a plugin's model you will have to add the `source` key (source: 'users-permissions')
        });
        return this.findOne({ id: entry.id });
      }

      entries.push(entry);
    }

    return entries;
  },
};
