"use strict";

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  lifecycles: {
    // Called after an entry is created
    beforeCreate(data) {
      //if the user didn't specified an id_card for his baby, a provisional id_card is created
      //composed by the user's username and the baby's id on database
      if (!data.id_card) {
        data.id_card = `${result.creator.username}-${result.id}`;
      }
    },
  },
};
