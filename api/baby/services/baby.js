"use strict";

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-services)
 * to customize this service
 */
const { isDraft } = require("strapi-utils").contentTypes;

module.exports = {
  getAllBabyRelations(params, populate) {
    populate = [
      "image",
      "doctors.meta_user.image",
      "injected_vaccines",
      "injected_vaccines.vaccine",
      "suffered_diseases",
      "suffered_diseases.disease",
      "blood_type",
      "measures",
      "injuries",
      "weights",
      "cranial_pressures",
      "insurances",
    ];
    return strapi.query("baby").findOne(params, populate);
  },
  findOne(params, populate) {
    populate = [
      "image",
      "doctors.meta_user.image",
      "doctors.meta_doctor.speciality",
      "measures",
      "injuries",
      "weights",
      "cranial_pressures",
      "insurances",
    ];
    return strapi.query("baby").findOne(params, populate);
  },

  find(params, populate) {
    populate = [
      "image",
      "doctors.meta_user.image",
      "doctors.meta_doctor.speciality",
      "creator.first_name",
      "creator.phone",
      "measures",
      "injuries",
      "weights",
      "cranial_pressures",
      "insurances",
    ];
    return strapi.query("baby").find(params, populate);
  },
  async create(data, { files } = {}) {
    //if the user didn't specified an id_card for his baby, a provisional id_card is created
    //composed by the user's username and the number of the amount of babies created plus 1
    if (!data.id_card) {
      const babiesPerUser = await strapi
        .query("baby")
        .count({ creator: data.creator });

      const creator = await strapi
        .query("user", "users-permissions")
        .findOne({ id: data.creator });

      data.id_card = `${creator.username}-${babiesPerUser + 1}`;
    }

    const validData = await strapi.entityValidator.validateEntityCreation(
      strapi.models.baby,
      data,
      { isDraft: isDraft(data, strapi.models.baby) }
    );

    const entry = await strapi.query("baby").create(validData);

    if (files) {
      // automatically uploads the files based on the entry and the model
      await strapi.entityService.uploadFiles(entry, files, {
        model: "baby",
        // if you are using a plugin's model you will have to add the `source` key (source: 'users-permissions')
      });
      return this.findOne({ id: entry.id });
    }

    return entry;
  },
  async update(params, data, { files } = {}) {
    const existingEntry = await strapi.query("baby").findOne(params);

    //if the user didn't specified an id_card for his baby, a provisional id_card is created
    //composed by the user's username and the baby's id on database
    if (!data.id_card) {
      data.id_card = `${existingEntry.creator.username}-${existingEntry.id}`;
    }

    const drafted = isDraft(existingEntry, strapi.models.baby);
    const validData = await strapi.entityValidator.validateEntityUpdate(
      strapi.models.baby,
      data,
      { drafted }
    );

    const entry = await strapi.query("baby").update(params, validData);

    if (files) {
      // automatically uploads the files based on the entry and the model
      await strapi.entityService.uploadFiles(entry, files, {
        model: "baby",
        // if you are using a plugin's model you will have to add the `source` key (source: 'users-permissions')
      });
      return this.findOne({ id: entry.id });
    }
    // const populate = [
    //   "image",
    //   "doctors.meta_doctor.speciality",
    //   "doctors.meta_user.image",
    // ];
    // const fullUpdatedEntry = await strapi
    //   .query("baby")
    //   .findOne(params, populate);

    const populate = [
      "doctors.meta_user.image",
      "doctors.meta_doctor.speciality",
    ];

    const updatedEntry = await strapi.query("baby").findOne(params, populate);

    return updatedEntry;
  },
};
