module.exports = ({ env }) => ({
  // ...
  email: {
    provider: "nodemailer",
    providerOptions: {
      host: env("SMTP_HOST", "smtp.ethereal.email"),
      port: env("SMTP_PORT", 587),
      auth: {
        user: env("SMTP_USER", "asia.braun83@ethereal.email"),
        pass: env("SMTP_PASS", "VwVxxnDfgYvt6hCwne"),
      },
      tls: {
        rejectUnauthorized: false,
      },
      // ... any custom nodemailer options
    },
    settings: {
      defaultFrom: "asia.braun83@ethereal.email",
      defaultReplyTo: "asia.braun83@ethereal.email",
    },
  },
  // ...
});
