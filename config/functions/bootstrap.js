"use strict";

/**
 * An asynchronous bootstrap function that runs before
 * your application gets started.
 *
 * This gives you an opportunity to set up your data model,
 * run jobs, or perform some special logic.
 *
 * See more details here: https://strapi.io/documentation/developer-docs/latest/setup-deployment-guides/configurations.html#bootstrap
 */

module.exports = () => {
  var io = require("socket.io")(strapi.server, {
    cors: {
      origin: "http://10.0.0.195:3000",
      methods: ["GET", "POST"],
      allowedHeaders: ["my-custom-header"],
      credentials: true,
    },
  });

  io.on("connection", function (socket) {
    socket.on("DoctorScanned", (socketId, babyId) => {
      console.log(socketId);
      io.to(socketId).emit("scanned", babyId);
    });
    socket.on("babyUpdated", (socketId) => {
      console.log(socketId);
      io.to(socketId).emit("updateBaby");
    });
  });
};
