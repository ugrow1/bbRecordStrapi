module.exports = ({ env }) => ({
  host: env("HOST", "0.0.0.0"),
  port: env.int("PORT", 1337),
  // url: env("URL", "http://10.0.0.195/"),
  admin: {
    auth: {
      secret: env("ADMIN_JWT_SECRET", "3c885faf785e42f0bf69255128c5fe3b"),
    },
  },
});
