module.exports = ({ env }) => ({
  defaultConnection: "default",
  connections: {
    default: {
      connector: "bookshelf",
      settings: {
        client: "mysql",
        timezone: "utc-4",
        host: env("DATABASE_HOST", "localhost"),
        port: env.int("DATABASE_PORT", 3306),
        database: env("DATABASE_NAME", "rtls_ugrowpx_002"),
        username: env("DATABASE_USERNAME", "root"),
        password: env("DATABASE_PASSWORD", "@Hotmail.com1"),
      },
      options: {
        useNullAsDefault: true,
      },
    },
  },
});
